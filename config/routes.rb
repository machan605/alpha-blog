Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #crea solo la ruta para la pagina principal
  root 'pages#home'

  #crea solo la ruta para la vista about
  get 'about', to: 'pages#about'

  #crea todas las rutas para los articulos
  resources :articles

  get 'signup', to: 'users#new' #llama la ruta signup pero pasa atravez de users controller y la action new
  #post 'users' to: 'users#create' #primer metodo para crear ruta unica es el primer metodo
  resources 'users', except: [:new] #crea las rutas, pero como ya se creo new con una ruta a parte se agrega la ecepcion

  #solo llama la ruta para el navegador de login y logout
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  #crea las rutas de categorias aciendo una ecepcion para el metodo destroy ya que no se necesita
  resources :categories, except: [:destroy]
end
