class User < ActiveRecord::Base
  has_many :articles, dependent: :destroy #dependent destruye los articulos que esten relacionados con el usuario cuand el usuario es destruido
  before_save { self.email = email.downcase } #esta line toma el valor y lo combierte en minusculas antes de salvar
  validates :username, presence: true,
            uniqueness: {case_sensitive: false},
            length: {minimum: 3, maximum: 25}
  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i #linea que pasa los valores para el formato email
  validates :email, presence: true,
            length: {minimum: 3, maximum: 105},
            uniqueness: {case_sensitive: false},
            format: { with: VALID_EMAIL_REGEX } #llama al formato de arriba para hacer la validacion
   has_secure_password
end
