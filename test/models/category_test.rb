require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def setup
    @category = Category.new(name: "sports")
  end

  test "categoria debe ser valida" do
    assert @category.valid?
  end

  test "nombre debe estar" do
    @category.name =" "
    assert_not @category.valid?
  end

  test "nombre debe ser unico" do
    @category.save
    category2 = Category.new(name: "sports")
    assert_not category2.valid?
  end

  test "nombre no tiene que ser muy largo" do
    @category.name = "a" * 26
    assert_not @category.valid?
  end

  test "nombre no tiene que ser corto" do
    @category.name = "aa"
    assert_not @category.valid?
  end
end
