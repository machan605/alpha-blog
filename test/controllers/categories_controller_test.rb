
require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @category = Category.create(name: "sports")
    @user = User.create(username: "julio", email:"j@gmai.com", password: "password", admin: true)
  end

  test "debe ingresar a index" do
    get categories_path
    assert_response :success
  end

  test "debe ingresar a new" do
    sing_in_as(@user, "password")
    get new_category_path
    assert_response :success
  end

  test "debe ingresar a show" do
    get category_path(@category)
    assert_response :success
  end

  test "debe redireccionar cunado el admin no esta conectado" do
    assert_no_difference 'Category.count'do
      post categories_path, params:{ category_path:{name: "sports"}}
    end
    assert_redirected_to categories_path
  end
end
